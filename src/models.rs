use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone, Copy, PartialEq)]
pub enum H {
    M,
    P,
    T,
}

#[derive(Deserialize)]
pub struct Input {
    pub a: bool,
    pub b: bool,
    pub c: bool,
    pub d: f32,
    pub e: i32,
    pub f: i32,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct Output {
    pub h: H,
    pub k: f32,
}
