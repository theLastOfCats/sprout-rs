use actix_web::{HttpResponse, web};

use crate::models::Input;
use crate::rules::process;

pub async fn calculate(data: web::Json<Input>) -> Result<HttpResponse, HttpResponse> {
    match process(data.into_inner()) {
        Ok(output) => Ok(HttpResponse::Ok().json(output)),
        Err(e) => Err(HttpResponse::BadRequest().body(e)),
    }
}