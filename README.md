# sprout-rs

# Architecture
In order to solve this, issue I try to use Rust and actix-web framework.
All the code stored in `src/` folder and split between 5 files:
* `main.rs` Main web server entry point
* `config.rs` Helpers for proper web-service configuration
* `handlers.rs` Endpoint handlers
* `models.rs` Models using for serialization/deserialization
* `rules.rs` Rules for substitution

And since only business logic is in rules, I used Rust testing capabilities and wrote test near code.
Plus, I used `rstest` library which help with input parametrization.

# Step-by-step resolving
0. Figure out data models
0. Add data substitution rules
0. Added test for data substitution rules
0. Wrote simple web server without configuration
0. Added Config acquiring from env
0. Added logging
0. Cleanup
0. GitLab CI

# How to build
0. Install [Rust](https://www.rust-lang.org/tools/install)
0. Clone code
0. `cargo build --release`
0. Run `target/release/sprout-rs`

# How to run tests
0. Install [Rust](https://www.rust-lang.org/tools/install)
0. Clone code
0. `cargo test`
