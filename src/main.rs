use actix_web::{App, HttpServer, middleware::Logger, web};
use color_eyre::Result;
use tracing::{info, instrument};

use crate::config::Config;

mod config;
mod handlers;
mod models;
mod rules;


#[actix_rt::main]
#[instrument]
async fn main() -> Result<()> {
    let config = Config::from_env().expect("Server configuration");

    info!("Starting server at http://{}:{}/", config.host, config.port);

    HttpServer::new(|| {
        App::new()
            .wrap(Logger::default())
            .route("/calculate", web::post().to(handlers::calculate))
    })
    .bind(format!("{}:{}", config.host, config.port))?
    .run()
    .await?;

    Ok(())
}
