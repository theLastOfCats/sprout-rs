use crate::models::{H, Input, Output};

fn custom_bool_rules(a: bool, b: bool, c: bool) -> Option<H> {
    match (a, b, c) {
        (true, true, false) => Some(H::T),
        (true, false, true) => Some(H::M),
        _ => None,
    }
}

fn base_bool_rules(a: bool, b: bool, c: bool) -> Option<H> {
    match (a, b, c) {
        (true, true, false) => Some(H::M),
        (true, true, true) => Some(H::P),
        (false, true, true) => Some(H::T),
        _ => None,
    }
}

fn custom_numeric_rules(h: H, d: f32, e: i32, f: i32) -> Option<f32> {
    match h {
        H::P => Some(2 as f32 * d + (d * e as f32 / 100 as f32)),
        H::M => Some(f as f32 + d + (d * e as f32 / 100 as f32)),
        _ => None,
    }
}

fn base_numeric_rules(h: H, d: f32, e: i32, f: i32) -> f32 {
    match h {
        H::M => d + (d * e as f32 / 10 as f32),
        H::P => d + (d * (e as f32 - f as f32) / 25.5),
        H::T => d - (d * f as f32 / 30 as f32),
    }
}

fn process_bool(a: bool, b: bool, c: bool) -> Option<H> {
    if let Some(custom) = custom_bool_rules(a, b, c) {
        return Some(custom);
    }

    base_bool_rules(a, b, c)
}

fn process_numeric(h: H, d: f32, e: i32, f: i32) -> f32 {
    if let Some(custom) = custom_numeric_rules(h, d, e, f) {
        return custom;
    }

    base_numeric_rules(h, d, e, f)
}

pub fn process(data: Input) -> Result<Output, &'static str> {
    if let Some(h) = process_bool(data.a, data.b, data.c) {
        let k = process_numeric(h, data.d, data.e, data.f);
        Ok(Output { h, k })
    } else {
        Err("Error!")
    }
}

#[cfg(test)]
mod tests {
    use rstest::rstest;

    use super::*;

    #[rstest(
    input, expected,
    case((true, true, true), None),
    case((false, false, false), None),
    case((true, false, false), None),
    case((false, true, false), None),
    case((false, false, true), None),
    case((false, true, true), None),
    case((true, true, false), Some(H::T)),
    case((true, false, true), Some(H::M)),
    )]
    fn test_custom_bool_rules(input: (bool, bool, bool), expected: Option<H>) {
        assert_eq!(expected, custom_bool_rules(input.0, input.1, input.2))
    }

    #[rstest(
    input, expected,
    case((true, true, true), Some(H::P)),
    case((false, false, false), None),
    case((true, false, false), None),
    case((false, true, false), None),
    case((false, false, true), None),
    case((false, true, true), Some(H::T)),
    case((true, true, false), Some(H::M)),
    case((true, false, true), None),
    )]
    fn test_base_bool_rules(input: (bool, bool, bool), expected: Option<H>) {
        assert_eq!(expected, base_bool_rules(input.0, input.1, input.2))
    }

    #[rstest(
    h, expected,
    case(H::M, Some(700.0)),
    case(H::P, Some(900.0)),
    case(H::T, None),
    )]
    fn test_custom_numeric_rules(h: H, expected: Option<f32>) {
        assert_eq!(expected, custom_numeric_rules(h, 300.0, 100, 100))
    }

    #[rstest(
    h, expected,
    case(H::M, 3300.0),
    case(H::P, 300.0),
    case(H::T, - 700.0),
    )]
    fn test_base_numeric_rules(h: H, expected: f32) {
        assert_eq!(expected, base_numeric_rules(h, 300.0, 100, 100))
    }

    #[rstest(
    input, expected,
    case((true, true, true), Some(H::P)),
    case((false, false, false), None),
    case((true, false, false), None),
    case((false, true, false), None),
    case((false, false, true), None),
    case((false, true, true), Some(H::T)),
    case((true, true, false), Some(H::T)),
    case((true, false, true), Some(H::M)),
    )]
    fn test_process_bool(input: (bool, bool, bool), expected: Option<H>) {
        assert_eq!(expected, process_bool(input.0, input.1, input.2))
    }

    #[rstest(
    h, expected,
    case(H::M, 700.0),
    case(H::P, 900.0),
    case(H::T, -700.0),
    )]
    fn test_process_numeric(h: H, expected: f32) {
        assert_eq!(expected, process_numeric(h, 300.0, 100, 100))
    }

    #[rstest(
    input, expected,
    case(Input {a: true, b: true, c: true, d: 300.0, e: 100, f: 100}, Output {h: H::P, k: 900.0}),
    case(Input {a: false, b: true, c: true, d: 300.0, e: 100, f: 100}, Output {h: H::T, k: -700.0}),
    case(Input {a: true, b: true, c: false, d: 300.0, e: 100, f: 100}, Output {h: H::T, k: -700.0}),
    case(Input {a: true, b: false, c: true, d: 300.0, e: 100, f: 100}, Output {h: H::M, k: 700.0}),
    )]
    fn test_process(input: Input, expected: Output) {
        assert_eq!(expected, process(input).unwrap())
    }

    #[rstest(
    input, expected,
    #[should_panic(expected = "Error!")]
    case(Input {a: false, b: false, c: false, d: 300.0, e: 100, f: 100}, Output {h: H::P, k: 900.0}),
    #[should_panic(expected = "Error!")]
    case(Input {a: true, b: false, c: false, d: 300.0, e: 100, f: 100}, Output {h: H::T, k: -700.0}),
    #[should_panic(expected = "Error!")]
    case(Input {a: false, b: true, c: false, d: 300.0, e: 100, f: 100}, Output {h: H::T, k: -700.0}),
    #[should_panic(expected = "Error!")]
    case(Input {a: false, b: false, c: true, d: 300.0, e: 100, f: 100}, Output {h: H::M, k: 700.0}),
    )]
    fn test_incorrect_process(input: Input, expected: Output) {
        assert_eq!(expected, process(input).unwrap())
    }
}
